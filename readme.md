![](http://i.imgur.com/YaZeTnH.png)
>Aw... Not this time...


What's this?
------------
A realtime computer status monitor written in Python 3.

Why?
----
We needed a way to monitor when screens, keyboards and mice were disconnected on
the computer labs I work at. And all the available monitoring solutions were complex to implement.